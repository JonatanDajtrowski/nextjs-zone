module.exports = {
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/:path*',
        destination: `/:path*`,
      },
      {
        source: '/off',
        destination: `http://localhost:3000/off`,
      },
      {
        source: '/off/:path*',
        destination: `http://localhost:3000/off/:path*`,
      },
      {
        source: '/rec',
        destination: `http://localhost:4200/rec`,
      },
      {
        source: '/rec/:path*',
        destination: `http://localhost:4200/rec/:path*`,
      },
    ]
  },
}
