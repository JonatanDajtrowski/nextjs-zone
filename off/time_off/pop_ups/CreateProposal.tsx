import {useDispatch, useSelector} from "react-redux";
import {Modal} from "@material-ui/core";
import {CurrentModalNameEnum} from "../../store/initial_state/modal_initial_state";
import {undoCurrentModal} from "../../store/slices/modalSlice";
import {useRouter} from "next/router";

const CreateProposal = () => {
    const {currentOpenModal} = useSelector((state : any) => state.modals )
    const dispatch = useDispatch()
    const router = useRouter()

    return (
        <Modal
            open={currentOpenModal === CurrentModalNameEnum.CREATE_PROPOSAL}
            onClose={() => {
                dispatch(undoCurrentModal())
                router.back()
            }}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            style={{
                display: "flex",
                justifyContent : "center",
                alignItems : "center"
            }}
        >
            <div
                style={{
                    width: "50%",
                    height: "50%",
                    backgroundColor : "white",
                }}
            >
                CreateProposal
            </div>
        </Modal>
    )
}

export default CreateProposal