import {useDispatch, useSelector} from "react-redux";
import {Modal} from "@material-ui/core";
import {CurrentModalNameEnum} from "../../store/initial_state/modal_initial_state";
import {undoCurrentModal} from "../../store/slices/modalSlice";
import {useRouter} from "next/router";
import Link from "next/link";

const ReviewCreatedProposalModal = () => {
    const {currentOpenModal} = useSelector((state : any) => state.modals )
    const dispatch = useDispatch()
    const router = useRouter()

    return (
        <Modal
            open={currentOpenModal === CurrentModalNameEnum.REVIEW_CREATED_PROPOSAL}
            onClose={() => {
                dispatch(undoCurrentModal())
                router.back()
            }}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            style={{
                display: "flex",
                justifyContent : "center",
                alignItems : "center"
            }}
        >
            <div
                style={{
                    width: "50%",
                    height: "50%",
                    backgroundColor : "white",
                }}
            >
                ReviewCreatedProposalModal
                <Link href={`/Time_Off/${CurrentModalNameEnum.CANCEL_PROPOSAL}`}>
                    <h3>CancelProposalModal</h3>
                </Link>
                <Link href={`/Time_Off/${CurrentModalNameEnum.EDIT_PROPOSAL}`}>
                    <h3>EditProposalModal</h3>
                </Link>
            </div>
        </Modal>
    )
}

export default ReviewCreatedProposalModal