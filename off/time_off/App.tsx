import Link from "next/link"
import {CurrentModalNameEnum} from "../store/initial_state/modal_initial_state";

const App = () => {

    return (
        <div>
            <h1>Planer Nieobecności</h1>

            <Link href={`/Time_Off/${CurrentModalNameEnum.REVIEW_RECEIVED_PROPOSAL}`}>
                <h3>ReviewReceivedProposal</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.DECLINE_RECEIVED_PROPOSAL}`}>
                <h3>DeclineProposal</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.CREATE_PROPOSAL}`}>
                <h3>CreateProposal</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.REVIEW_CREATED_PROPOSAL}`}>
                <h3>ReviewCreatedProposalModal</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.CANCEL_PROPOSAL}`}>
                <h3>CancelProposalModal</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.EDIT_PROPOSAL}`}>
                <h3>EditProposalModal</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.DELETE_ABSENCE}`}>
                <h3>DeleteAbsence</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.REVIEW_ABSENCE}`}>
                <h3>ReviewAbsence</h3>
            </Link>
            <Link href={`/Time_Off/${CurrentModalNameEnum.ACCEPT_RECEIVED_PROPOSAL}`}>
                <h3>AcceptProposal</h3>
            </Link>

            <Link href={`/`}>
                <h3>off</h3>
            </Link>
        </div>
    )
}

export default App