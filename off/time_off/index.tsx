import App from "./App";
import ReviewReceivedProposal from "./pop_ups/ReviewReceivedProposal";
import React from "react";
import AcceptProposal from "./pop_ups/AcceptProposal";
import DeclineProposal from "./pop_ups/DeclineProposal";
import CreateProposal from "./pop_ups/CreateProposal";
import EditProposalModal from "./pop_ups/EditProposalModal";
import DeleteAbsence from "./pop_ups/DeleteAbsence";
import ReviewAbsence from "./pop_ups/ReviewAbsence";
import CancelProposalModal from "./pop_ups/CancelProposalModal";
import ReviewCreatedProposalModal from "./pop_ups/ReviewCreatedProposalModal";



const TimeOff = () => {
    return (
        <>
            <App />

            <ReviewReceivedProposal />
            <DeclineProposal />
            <CreateProposal />
            <ReviewCreatedProposalModal />
            <CancelProposalModal />
            <EditProposalModal />
            <DeleteAbsence />
            <ReviewAbsence />
            <AcceptProposal />
        </>
    )
}

export default TimeOff