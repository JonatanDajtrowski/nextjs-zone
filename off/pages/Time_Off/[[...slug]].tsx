import TimeOff from "../../time_off";
import {useRouter} from "next/router";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {setCurrentOpenModal} from "../../store/slices/modalSlice";


const TimeOffRouter = () => {

    const router = useRouter();
    const {slug} = router.query;
    const {currentOpenModal} = useSelector((state : any) => state.modals )
    const dispatch = useDispatch()

    useEffect(() => {
        console.log(slug,currentOpenModal,"slug")
        if(slug){
            dispatch(setCurrentOpenModal(slug))
        }else {
            dispatch(setCurrentOpenModal([null]))
        }
    },[slug,currentOpenModal])

    return (
        <TimeOff />
    )
}

export default  TimeOffRouter