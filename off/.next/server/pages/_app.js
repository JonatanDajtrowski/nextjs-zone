(function() {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./pages/_app.tsx":
/*!************************!*\
  !*** ./pages/_app.tsx ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-redux */ "react-redux");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _store_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store/store */ "./store/store.ts");

var _jsxFileName = "C:\\Users\\Admin\\Desktop\\Jonatan\\programowanie\\nextjs-zone\\off\\pages\\_app.tsx";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }





function MyApp({
  Component,
  pageProps
}) {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((react__WEBPACK_IMPORTED_MODULE_1___default().StrictMode), {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_redux__WEBPACK_IMPORTED_MODULE_2__.Provider, {
      store: _store_store__WEBPACK_IMPORTED_MODULE_3__.default,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, _objectSpread({}, pageProps), void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 13
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 11
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 9,
    columnNumber: 7
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (MyApp);

/***/ }),

/***/ "./store/initial_state/modal_initial_state.ts":
/*!****************************************************!*\
  !*** ./store/initial_state/modal_initial_state.ts ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentModalNameEnum": function() { return /* binding */ CurrentModalNameEnum; }
/* harmony export */ });
let CurrentModalNameEnum;

(function (CurrentModalNameEnum) {
  CurrentModalNameEnum["CREATE_PROPOSAL"] = "CreateProposalModal";
  CurrentModalNameEnum["REVIEW_CREATED_PROPOSAL"] = "ReviewCreatedProposalModal";
  CurrentModalNameEnum["EDIT_PROPOSAL"] = "EditProposalModal";
  CurrentModalNameEnum["CANCEL_PROPOSAL"] = "CancelProposalModal";
  CurrentModalNameEnum["REVIEW_RECEIVED_PROPOSAL"] = "ReviewReceivedProposalModal";
  CurrentModalNameEnum["DECLINE_RECEIVED_PROPOSAL"] = "DeclineReceivedProposalModal";
  CurrentModalNameEnum["ACCEPT_RECEIVED_PROPOSAL"] = "AcceptReceivedProposalModal";
  CurrentModalNameEnum["REVIEW_ABSENCE"] = "ReviewAbsenceModal";
  CurrentModalNameEnum["DELETE_ABSENCE"] = "DeleteAbsenceModal";
})(CurrentModalNameEnum || (CurrentModalNameEnum = {}));

const modalPopupInitialState = {
  modalHistory: [],
  currentProposal: [],
  currentAbsence: [],
  currentOpenModal: null,
  isHistoryBlock: false
};
/* harmony default export */ __webpack_exports__["default"] = (modalPopupInitialState);

/***/ }),

/***/ "./store/slices/modalSlice.ts":
/*!************************************!*\
  !*** ./store/slices/modalSlice.ts ***!
  \************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "undoCurrentModal": function() { return /* binding */ undoCurrentModal; },
/* harmony export */   "clearModalHistory": function() { return /* binding */ clearModalHistory; },
/* harmony export */   "setCurrentOpenModal": function() { return /* binding */ setCurrentOpenModal; },
/* harmony export */   "setHistoryBlock": function() { return /* binding */ setHistoryBlock; }
/* harmony export */ });
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ "@reduxjs/toolkit");
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _initial_state_modal_initial_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../initial_state/modal_initial_state */ "./store/initial_state/modal_initial_state.ts");


const modalPopupSlice = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: 'modalPopup',
  initialState: _initial_state_modal_initial_state__WEBPACK_IMPORTED_MODULE_1__.default,
  reducers: {
    setCurrentOpenModal(state, action) {
      if (state.currentOpenModal !== action.payload[0]) {
        state.modalHistory.push(state.currentOpenModal);
        state.currentOpenModal = action.payload[0];
      } else {
        state.currentOpenModal = action.payload[0];
      }
    },

    undoCurrentModal(state) {
      const undoModal = state.modalHistory.pop();
      console.log(undoModal, "undoModal");

      if (undoModal) {
        state.currentOpenModal = undoModal;
      } else {
        state.currentOpenModal = null;
      }
    },

    clearModalHistory(state) {
      state.currentOpenModal = null;
      state.modalHistory = [];
    },

    setHistoryBlock(state, action) {
      state.isHistoryBlock = action.payload.isHistoryBlock;
    }

  }
});
const {
  undoCurrentModal,
  clearModalHistory,
  setCurrentOpenModal,
  setHistoryBlock
} = modalPopupSlice.actions;
/* harmony default export */ __webpack_exports__["default"] = (modalPopupSlice.reducer);

/***/ }),

/***/ "./store/store.ts":
/*!************************!*\
  !*** ./store/store.ts ***!
  \************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @reduxjs/toolkit */ "@reduxjs/toolkit");
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _slices_modalSlice__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./slices/modalSlice */ "./store/slices/modalSlice.ts");


/* harmony default export */ __webpack_exports__["default"] = ((0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.configureStore)({
  reducer: {
    modals: _slices_modalSlice__WEBPACK_IMPORTED_MODULE_1__.default
  }
}));

/***/ }),

/***/ "@reduxjs/toolkit":
/*!***********************************!*\
  !*** external "@reduxjs/toolkit" ***!
  \***********************************/
/***/ (function(module) {

"use strict";
module.exports = require("@reduxjs/toolkit");;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ "react-redux":
/*!******************************!*\
  !*** external "react-redux" ***!
  \******************************/
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/_app.tsx"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9uZXh0anMtdGVzdC8uL3BhZ2VzL19hcHAudHN4Iiwid2VicGFjazovL25leHRqcy10ZXN0Ly4vc3RvcmUvaW5pdGlhbF9zdGF0ZS9tb2RhbF9pbml0aWFsX3N0YXRlLnRzIiwid2VicGFjazovL25leHRqcy10ZXN0Ly4vc3RvcmUvc2xpY2VzL21vZGFsU2xpY2UudHMiLCJ3ZWJwYWNrOi8vbmV4dGpzLXRlc3QvLi9zdG9yZS9zdG9yZS50cyIsIndlYnBhY2s6Ly9uZXh0anMtdGVzdC9leHRlcm5hbCBcIkByZWR1eGpzL3Rvb2xraXRcIiIsIndlYnBhY2s6Ly9uZXh0anMtdGVzdC9leHRlcm5hbCBcInJlYWN0XCIiLCJ3ZWJwYWNrOi8vbmV4dGpzLXRlc3QvZXh0ZXJuYWwgXCJyZWFjdC1yZWR1eFwiIiwid2VicGFjazovL25leHRqcy10ZXN0L2V4dGVybmFsIFwicmVhY3QvanN4LWRldi1ydW50aW1lXCIiXSwibmFtZXMiOlsiTXlBcHAiLCJDb21wb25lbnQiLCJwYWdlUHJvcHMiLCJzdG9yZSIsIkN1cnJlbnRNb2RhbE5hbWVFbnVtIiwibW9kYWxQb3B1cEluaXRpYWxTdGF0ZSIsIm1vZGFsSGlzdG9yeSIsImN1cnJlbnRQcm9wb3NhbCIsImN1cnJlbnRBYnNlbmNlIiwiY3VycmVudE9wZW5Nb2RhbCIsImlzSGlzdG9yeUJsb2NrIiwibW9kYWxQb3B1cFNsaWNlIiwiY3JlYXRlU2xpY2UiLCJuYW1lIiwiaW5pdGlhbFN0YXRlIiwicmVkdWNlcnMiLCJzZXRDdXJyZW50T3Blbk1vZGFsIiwic3RhdGUiLCJhY3Rpb24iLCJwYXlsb2FkIiwicHVzaCIsInVuZG9DdXJyZW50TW9kYWwiLCJ1bmRvTW9kYWwiLCJwb3AiLCJjb25zb2xlIiwibG9nIiwiY2xlYXJNb2RhbEhpc3RvcnkiLCJzZXRIaXN0b3J5QmxvY2siLCJhY3Rpb25zIiwicmVkdWNlciIsImNvbmZpZ3VyZVN0b3JlIiwibW9kYWxzIiwibW9kYWxSZWR1Y2VyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0EsS0FBVCxDQUFlO0FBQUVDLFdBQUY7QUFBYUM7QUFBYixDQUFmLEVBQW1EO0FBRWpELHNCQUNJLDhEQUFDLHlEQUFEO0FBQUEsMkJBQ0ksOERBQUMsaURBQUQ7QUFBVSxXQUFLLEVBQUdDLGlEQUFsQjtBQUFBLDZCQUNFLDhEQUFDLFNBQUQsb0JBQWVELFNBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFPRDs7QUFDRCwrREFBZUYsS0FBZixFOzs7Ozs7Ozs7Ozs7Ozs7QUNkTyxJQUFLSSxvQkFBWjs7V0FBWUEsb0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7QUFBQUEsc0I7R0FBQUEsb0IsS0FBQUEsb0I7O0FBb0JaLE1BQU1DLHNCQUF3QyxHQUFHO0FBQzdDQyxjQUFZLEVBQUcsRUFEOEI7QUFFN0NDLGlCQUFlLEVBQUUsRUFGNEI7QUFHN0NDLGdCQUFjLEVBQUcsRUFINEI7QUFJN0NDLGtCQUFnQixFQUFHLElBSjBCO0FBSzdDQyxnQkFBYyxFQUFHO0FBTDRCLENBQWpEO0FBUUEsK0RBQWVMLHNCQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCQTtBQUNBO0FBRUEsTUFBTU0sZUFBZSxHQUFHQyw2REFBVyxDQUFDO0FBQ2hDQyxNQUFJLEVBQUUsWUFEMEI7QUFFaENDLGNBQVksRUFBRVQsdUVBRmtCO0FBR2hDVSxVQUFRLEVBQUU7QUFDTkMsdUJBQW1CLENBQUNDLEtBQUQsRUFBUUMsTUFBUixFQUFlO0FBQzlCLFVBQUdELEtBQUssQ0FBQ1IsZ0JBQU4sS0FBMkJTLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlLENBQWYsQ0FBOUIsRUFBZ0Q7QUFDNUNGLGFBQUssQ0FBQ1gsWUFBTixDQUFtQmMsSUFBbkIsQ0FBd0JILEtBQUssQ0FBQ1IsZ0JBQTlCO0FBQ0FRLGFBQUssQ0FBQ1IsZ0JBQU4sR0FBeUJTLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlLENBQWYsQ0FBekI7QUFDSCxPQUhELE1BR007QUFDRkYsYUFBSyxDQUFDUixnQkFBTixHQUF5QlMsTUFBTSxDQUFDQyxPQUFQLENBQWUsQ0FBZixDQUF6QjtBQUNIO0FBQ0osS0FSSzs7QUFTTkUsb0JBQWdCLENBQUNKLEtBQUQsRUFBTztBQUNuQixZQUFNSyxTQUFTLEdBQUdMLEtBQUssQ0FBQ1gsWUFBTixDQUFtQmlCLEdBQW5CLEVBQWxCO0FBQ0FDLGFBQU8sQ0FBQ0MsR0FBUixDQUFZSCxTQUFaLEVBQXVCLFdBQXZCOztBQUNBLFVBQUdBLFNBQUgsRUFBYTtBQUNUTCxhQUFLLENBQUNSLGdCQUFOLEdBQXlCYSxTQUF6QjtBQUNILE9BRkQsTUFFTTtBQUNGTCxhQUFLLENBQUNSLGdCQUFOLEdBQXlCLElBQXpCO0FBQ0g7QUFDSixLQWpCSzs7QUFrQk5pQixxQkFBaUIsQ0FBQ1QsS0FBRCxFQUFPO0FBQ3BCQSxXQUFLLENBQUNSLGdCQUFOLEdBQXlCLElBQXpCO0FBQ0FRLFdBQUssQ0FBQ1gsWUFBTixHQUFxQixFQUFyQjtBQUNILEtBckJLOztBQXNCTnFCLG1CQUFlLENBQUNWLEtBQUQsRUFBT0MsTUFBUCxFQUFjO0FBQ3pCRCxXQUFLLENBQUNQLGNBQU4sR0FBdUJRLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlVCxjQUF0QztBQUNIOztBQXhCSztBQUhzQixDQUFELENBQW5DO0FBK0JPLE1BQU07QUFDVFcsa0JBRFM7QUFFVEssbUJBRlM7QUFHVFYscUJBSFM7QUFJVFc7QUFKUyxJQUtUaEIsZUFBZSxDQUFDaUIsT0FMYjtBQU1QLCtEQUFlakIsZUFBZSxDQUFDa0IsT0FBL0IsRTs7Ozs7Ozs7Ozs7Ozs7O0FDeENBO0FBQ0E7QUFFQSwrREFBZUMsZ0VBQWMsQ0FBRTtBQUMzQkQsU0FBTyxFQUFFO0FBQ0xFLFVBQU0sRUFBR0MsdURBQVlBO0FBRGhCO0FBRGtCLENBQUYsQ0FBN0IsRTs7Ozs7Ozs7Ozs7QUNIQSw4Qzs7Ozs7Ozs7Ozs7QUNBQSxtQzs7Ozs7Ozs7Ozs7QUNBQSx5Qzs7Ozs7Ozs7Ozs7QUNBQSxtRCIsImZpbGUiOiJwYWdlcy9fYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHR5cGUgeyBBcHBQcm9wcyB9IGZyb20gJ25leHQvYXBwJ1xyXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXHJcbmltcG9ydCB7UHJvdmlkZXJ9IGZyb20gXCJyZWFjdC1yZWR1eFwiO1xyXG5pbXBvcnQgc3RvcmUgZnJvbSBcIi4uL3N0b3JlL3N0b3JlXCJcclxuXHJcbmZ1bmN0aW9uIE15QXBwKHsgQ29tcG9uZW50LCBwYWdlUHJvcHMgfTogQXBwUHJvcHMpIHtcclxuXHJcbiAgcmV0dXJuIChcclxuICAgICAgPFJlYWN0LlN0cmljdE1vZGU+XHJcbiAgICAgICAgICA8UHJvdmlkZXIgc3RvcmU9eyBzdG9yZSB9PlxyXG4gICAgICAgICAgICA8Q29tcG9uZW50IHsuLi5wYWdlUHJvcHN9IC8+XHJcbiAgICAgICAgICA8L1Byb3ZpZGVyPlxyXG4gICAgICA8L1JlYWN0LlN0cmljdE1vZGU+XHJcbiAgKVxyXG59XHJcbmV4cG9ydCBkZWZhdWx0IE15QXBwXHJcbiIsIlxyXG5leHBvcnQgZW51bSBDdXJyZW50TW9kYWxOYW1lRW51bSB7XHJcbiAgICBDUkVBVEVfUFJPUE9TQUwgPSBcIkNyZWF0ZVByb3Bvc2FsTW9kYWxcIixcclxuICAgIFJFVklFV19DUkVBVEVEX1BST1BPU0FMID0gXCJSZXZpZXdDcmVhdGVkUHJvcG9zYWxNb2RhbFwiLFxyXG4gICAgRURJVF9QUk9QT1NBTCA9IFwiRWRpdFByb3Bvc2FsTW9kYWxcIixcclxuICAgIENBTkNFTF9QUk9QT1NBTCA9IFwiQ2FuY2VsUHJvcG9zYWxNb2RhbFwiLFxyXG4gICAgUkVWSUVXX1JFQ0VJVkVEX1BST1BPU0FMID0gXCJSZXZpZXdSZWNlaXZlZFByb3Bvc2FsTW9kYWxcIixcclxuICAgIERFQ0xJTkVfUkVDRUlWRURfUFJPUE9TQUwgPSBcIkRlY2xpbmVSZWNlaXZlZFByb3Bvc2FsTW9kYWxcIixcclxuICAgIEFDQ0VQVF9SRUNFSVZFRF9QUk9QT1NBTCA9IFwiQWNjZXB0UmVjZWl2ZWRQcm9wb3NhbE1vZGFsXCIsXHJcbiAgICBSRVZJRVdfQUJTRU5DRSA9IFwiUmV2aWV3QWJzZW5jZU1vZGFsXCIsXHJcbiAgICBERUxFVEVfQUJTRU5DRSA9IFwiRGVsZXRlQWJzZW5jZU1vZGFsXCIsXHJcbn1cclxuXHJcbmV4cG9ydCB0eXBlIG1vZGFsUG9wdXBTdGF0ZSA9IHtcclxuICAgIG1vZGFsSGlzdG9yeSA6IChzdHJpbmcgfCBudWxsKVtdLFxyXG4gICAgY3VycmVudFByb3Bvc2FsIDogYW55LFxyXG4gICAgY3VycmVudEFic2VuY2UgOiBhbnksXHJcbiAgICBjdXJyZW50T3Blbk1vZGFsIDogQ3VycmVudE1vZGFsTmFtZUVudW0gfCBudWxsLFxyXG4gICAgaXNIaXN0b3J5QmxvY2sgOiBib29sZWFuXHJcbn1cclxuXHJcbmNvbnN0IG1vZGFsUG9wdXBJbml0aWFsU3RhdGUgOiBtb2RhbFBvcHVwU3RhdGUgPSB7XHJcbiAgICBtb2RhbEhpc3RvcnkgOiBbXSxcclxuICAgIGN1cnJlbnRQcm9wb3NhbDogW10sXHJcbiAgICBjdXJyZW50QWJzZW5jZSA6IFtdLFxyXG4gICAgY3VycmVudE9wZW5Nb2RhbCA6IG51bGwsXHJcbiAgICBpc0hpc3RvcnlCbG9jayA6IGZhbHNlXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IG1vZGFsUG9wdXBJbml0aWFsU3RhdGUiLCJpbXBvcnQge2NyZWF0ZVNsaWNlfSBmcm9tIFwiQHJlZHV4anMvdG9vbGtpdFwiO1xyXG5pbXBvcnQgbW9kYWxQb3B1cEluaXRpYWxTdGF0ZSwge0N1cnJlbnRNb2RhbE5hbWVFbnVtfSBmcm9tIFwiLi4vaW5pdGlhbF9zdGF0ZS9tb2RhbF9pbml0aWFsX3N0YXRlXCI7XHJcblxyXG5jb25zdCBtb2RhbFBvcHVwU2xpY2UgPSBjcmVhdGVTbGljZSh7XHJcbiAgICBuYW1lOiAnbW9kYWxQb3B1cCcsXHJcbiAgICBpbml0aWFsU3RhdGU6IG1vZGFsUG9wdXBJbml0aWFsU3RhdGUsXHJcbiAgICByZWR1Y2Vyczoge1xyXG4gICAgICAgIHNldEN1cnJlbnRPcGVuTW9kYWwoc3RhdGUsIGFjdGlvbil7XHJcbiAgICAgICAgICAgIGlmKHN0YXRlLmN1cnJlbnRPcGVuTW9kYWwgIT09IGFjdGlvbi5wYXlsb2FkWzBdKXtcclxuICAgICAgICAgICAgICAgIHN0YXRlLm1vZGFsSGlzdG9yeS5wdXNoKHN0YXRlLmN1cnJlbnRPcGVuTW9kYWwpXHJcbiAgICAgICAgICAgICAgICBzdGF0ZS5jdXJyZW50T3Blbk1vZGFsID0gYWN0aW9uLnBheWxvYWRbMF07XHJcbiAgICAgICAgICAgIH1lbHNlIHtcclxuICAgICAgICAgICAgICAgIHN0YXRlLmN1cnJlbnRPcGVuTW9kYWwgPSBhY3Rpb24ucGF5bG9hZFswXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdW5kb0N1cnJlbnRNb2RhbChzdGF0ZSl7XHJcbiAgICAgICAgICAgIGNvbnN0IHVuZG9Nb2RhbCA9IHN0YXRlLm1vZGFsSGlzdG9yeS5wb3AoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codW5kb01vZGFsLCBcInVuZG9Nb2RhbFwiKVxyXG4gICAgICAgICAgICBpZih1bmRvTW9kYWwpe1xyXG4gICAgICAgICAgICAgICAgc3RhdGUuY3VycmVudE9wZW5Nb2RhbCA9IHVuZG9Nb2RhbCBhcyBDdXJyZW50TW9kYWxOYW1lRW51bTtcclxuICAgICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICAgICAgc3RhdGUuY3VycmVudE9wZW5Nb2RhbCA9IG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGNsZWFyTW9kYWxIaXN0b3J5KHN0YXRlKXtcclxuICAgICAgICAgICAgc3RhdGUuY3VycmVudE9wZW5Nb2RhbCA9IG51bGw7XHJcbiAgICAgICAgICAgIHN0YXRlLm1vZGFsSGlzdG9yeSA9IFtdXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzZXRIaXN0b3J5QmxvY2soc3RhdGUsYWN0aW9uKXtcclxuICAgICAgICAgICAgc3RhdGUuaXNIaXN0b3J5QmxvY2sgPSBhY3Rpb24ucGF5bG9hZC5pc0hpc3RvcnlCbG9ja1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbn0pXHJcblxyXG5leHBvcnQgY29uc3Qge1xyXG4gICAgdW5kb0N1cnJlbnRNb2RhbCxcclxuICAgIGNsZWFyTW9kYWxIaXN0b3J5LFxyXG4gICAgc2V0Q3VycmVudE9wZW5Nb2RhbCxcclxuICAgIHNldEhpc3RvcnlCbG9jayxcclxufSA9IG1vZGFsUG9wdXBTbGljZS5hY3Rpb25zXHJcbmV4cG9ydCBkZWZhdWx0IG1vZGFsUG9wdXBTbGljZS5yZWR1Y2VyIiwiaW1wb3J0IHsgY29uZmlndXJlU3RvcmUgfSBmcm9tICdAcmVkdXhqcy90b29sa2l0JztcclxuaW1wb3J0IG1vZGFsUmVkdWNlciBmcm9tIFwiLi9zbGljZXMvbW9kYWxTbGljZVwiXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjb25maWd1cmVTdG9yZSgge1xyXG4gICAgcmVkdWNlcjoge1xyXG4gICAgICAgIG1vZGFscyA6IG1vZGFsUmVkdWNlclxyXG4gICAgfVxyXG59ICkiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAcmVkdXhqcy90b29sa2l0XCIpOzsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVhY3QtcmVkdXhcIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTs7Il0sInNvdXJjZVJvb3QiOiIifQ==