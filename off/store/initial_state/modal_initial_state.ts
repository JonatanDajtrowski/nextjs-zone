
export enum CurrentModalNameEnum {
    CREATE_PROPOSAL = "CreateProposalModal",
    REVIEW_CREATED_PROPOSAL = "ReviewCreatedProposalModal",
    EDIT_PROPOSAL = "EditProposalModal",
    CANCEL_PROPOSAL = "CancelProposalModal",
    REVIEW_RECEIVED_PROPOSAL = "ReviewReceivedProposalModal",
    DECLINE_RECEIVED_PROPOSAL = "DeclineReceivedProposalModal",
    ACCEPT_RECEIVED_PROPOSAL = "AcceptReceivedProposalModal",
    REVIEW_ABSENCE = "ReviewAbsenceModal",
    DELETE_ABSENCE = "DeleteAbsenceModal",
}

export type modalPopupState = {
    modalHistory : (string | null)[],
    currentProposal : any,
    currentAbsence : any,
    currentOpenModal : CurrentModalNameEnum | null,
    isHistoryBlock : boolean
}

const modalPopupInitialState : modalPopupState = {
    modalHistory : [],
    currentProposal: [],
    currentAbsence : [],
    currentOpenModal : null,
    isHistoryBlock : false
}

export default modalPopupInitialState