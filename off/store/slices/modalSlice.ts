import {createSlice} from "@reduxjs/toolkit";
import modalPopupInitialState, {CurrentModalNameEnum} from "../initial_state/modal_initial_state";

const modalPopupSlice = createSlice({
    name: 'modalPopup',
    initialState: modalPopupInitialState,
    reducers: {
        setCurrentOpenModal(state, action){
            if(state.currentOpenModal !== action.payload[0]){
                state.modalHistory.push(state.currentOpenModal)
                state.currentOpenModal = action.payload[0];
            }else {
                state.currentOpenModal = action.payload[0];
            }
        },
        undoCurrentModal(state){
            const undoModal = state.modalHistory.pop();
            console.log(undoModal, "undoModal")
            if(undoModal){
                state.currentOpenModal = undoModal as CurrentModalNameEnum;
            }else {
                state.currentOpenModal = null;
            }
        },
        clearModalHistory(state){
            state.currentOpenModal = null;
            state.modalHistory = []
        },
        setHistoryBlock(state,action){
            state.isHistoryBlock = action.payload.isHistoryBlock
        }
    },
})

export const {
    undoCurrentModal,
    clearModalHistory,
    setCurrentOpenModal,
    setHistoryBlock,
} = modalPopupSlice.actions
export default modalPopupSlice.reducer